package us.codecraft.webmagic.lianjia.processor;

import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Record;
import us.codecraft.webmagic.Page;
import us.codecraft.webmagic.Site;
import us.codecraft.webmagic.lianjia.model.AreaPage;
import us.codecraft.webmagic.lianjia.model.HouseUrl;
import us.codecraft.webmagic.processor.PageProcessor;
import us.codecraft.webmagic.selector.Selectable;

import java.util.Date;
import java.util.List;

public class SecondHandAreaNumProcessor implements PageProcessor {
    private Site site = Site
            .me()
            .setDomain("sh.lianjia.com")
            .setSleepTime(1000)
            .setUserAgent(
                    "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_2) AppleWebKit/537.31 (KHTML, like Gecko) Chrome/26.0.1410.65 Safari/537.31");

    public void process(Page page) {
        if (page.getRequest().getUrl().contains("pg")) {
            String areaUrl = page.getRequest().getUrl();
            String area = areaUrl.substring(34, areaUrl.lastIndexOf("/"));
            Record areaPage = Db.findFirst("select * from t_sh_second_sale_area_page where url = '" + areaUrl + "'");
            if (areaPage.getStr("state").equals("1")) {
                return;
            }
            List<Selectable> uls = page.getHtml().$(".sellListContent li").nodes();
            for (Selectable ul : uls) {
                String url = ul.$(".title").links().regex("(https://sh.lianjia.com/ershoufang/[0-9]+.html)").all().get(0);
                String name = ul.$(".title a", "text").toString();
                String address = ul.$(".houseInfo", "text").toString();
                String flood = ul.$(".positionInfo", "text").toString();
                String followInfo = ul.$(".followInfo", "text").toString();
                String community = ul.$(".houseInfo a", "text").toString();
                String communityUrl = ul.$(".houseInfo a").links().all().get(0);
                String totalPrice = ul.$(".totalPrice span", "text").toString();
                String unitPrice = ul.$(".unitPrice span", "text").toString();
                new HouseUrl()
                        .set("target_url", url)
                        .set("iid", url.substring(34, url.lastIndexOf(".")))
                        .set("area", area)
                        .set("name", name)
                        .set("total_price", totalPrice)
                        .set("unit_price", unitPrice)
                        .set("community", community)
                        .set("community_url", communityUrl)
                        .set("address", address)
                        .set("flood", flood)
                        .set("followInfo", followInfo)
                        .set("update_time", new Date())
                        .save();
            }

            areaPage.set("update_time", new Date());
            areaPage.set("state", 1);
            Db.update("t_sh_second_sale_area_page", areaPage);
        } else {
            String area = page.getRequest().getUrl().substring(34).replace("/", "");
            String areaName = page.getHtml().$("div.position > dl:nth-child(2) > dd > div:nth-child(1) > div:nth-child(2) > a.selected", "text").toString().trim();
            String areaNum = page.getHtml().$("body > div.content > div.leftContent > div.resultDes.clear > h2 > span", "text").toString().trim();
            Record areaR = Db.findFirst("select * from t_sh_second_sale_area where area = ?", area);
            areaR.set("area_name", areaName);
            areaR.set("num", areaNum);
            areaR.set("update_time", new Date());
            Db.update("t_sh_second_sale_area", areaR);

            int num = Integer.parseInt(areaNum);
            int pageNum = (int) Math.ceil(num / 30);
            for (int i = 1; i <= pageNum; i++) {
                String url = page.getRequest().getUrl() + "pg" + i + "co32";
                Record areaPage = Db.findFirst("select * from t_sh_second_sale_area_page where url = '" + url + "'");
                if (url == null) {
                    new AreaPage()
                            .set("area", area)
                            .set("page", i)
                            .set("url", page.getRequest().getUrl() + "pg" + i + "co32")
                            .set("update_time", new Date())
                            .save();
                    page.addTargetRequest(page.getRequest().getUrl() + "pg" + i + "co32");
                } else {
                    if (areaPage.getStr("state").equals("0")) {
                        page.addTargetRequest(page.getRequest().getUrl() + "pg" + i + "co32");
                    }
                }

            }
        }

    }

    public Site getSite() {
        return site;
    }
}
