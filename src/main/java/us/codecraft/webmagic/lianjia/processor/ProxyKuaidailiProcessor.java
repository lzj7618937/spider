package us.codecraft.webmagic.lianjia.processor;

import us.codecraft.webmagic.Page;
import us.codecraft.webmagic.Site;
import us.codecraft.webmagic.processor.PageProcessor;
import us.codecraft.webmagic.selector.Selectable;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

//快代理
//https://www.kuaidaili.com/free/inha/
public class ProxyKuaidailiProcessor implements PageProcessor {

    Map<String, Integer> proxyIpMap = new HashMap<String, Integer>();

    private Site site = Site
            .me()
            .setDomain("www.kuaidaili.com")
            .setSleepTime(1000)
            .setUserAgent(
                    "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_2) AppleWebKit/537.31 (KHTML, like Gecko) Chrome/26.0.1410.65 Safari/537.31");

    @Override
    public void process(Page page) {
        List<Selectable> tds = page.getHtml().$("tbody tr").nodes();
        for (Selectable td : tds) {
            String ip = td.xpath("//td[@data-title='IP']/text()").toString();
            String port = td.xpath("//td[@data-title='PORT']/text()").toString();
            proxyIpMap.put(ip,Integer.parseInt(port));
        }
    }

    @Override
    public Site getSite() {
        return site;
    }
}
