package us.codecraft.webmagic.lianjia.processor;

import us.codecraft.webmagic.Page;
import us.codecraft.webmagic.Site;
import us.codecraft.webmagic.Spider;
import us.codecraft.webmagic.pipeline.ConsolePipeline;
import us.codecraft.webmagic.processor.PageProcessor;
import us.codecraft.webmagic.selector.Selectable;

import java.util.List;

public class GetDetailListProcessor implements PageProcessor {


    private Site site;

    private String domain;

    private String ulsSelectable;

    private String nameSelectable;
    private String urlSelectable;

    public GetDetailListProcessor(String domainUrl, String ulsSelectable, String nameSelectable, String urlSelectable) {
        this.domain = domainUrl;
        this.ulsSelectable = ulsSelectable;
        this.nameSelectable = nameSelectable;
        this.urlSelectable = urlSelectable;
        this.site = Site
                .me()
                .setDomain(domainUrl)
                .setSleepTime(3000)
                .setUserAgent(
                        "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_2) AppleWebKit/537.31 (KHTML, like Gecko) Chrome/26.0.1410.65 Safari/537.31");
    }

    public void process(Page page) {
        List<Selectable> urls = page.getHtml().$(ulsSelectable).nodes();
        for (Selectable ul : urls) {
            String url = domain + ul.xpath(urlSelectable).toString();
            String name = ul.xpath(nameSelectable).toString();
            System.out.println(name + "[" + url + "]");
        }
    }

    public Site getSite() {
        return site;
    }

    public static void main(String[] args) {
        String domain = "https://sh.lianjia.com";
        //获取二级分类
        String url = "https://sh.lianjia.com/ershoufang/biyun/";
        String ulsSelectable = "div.leftContent > div.contentBottom.clear > div.page-box.fr > div";

        String nameSelectable = "//div/@page-data";
        String urlSelectable = "//a/@href";

        Spider.create(new GetDetailListProcessor(domain, ulsSelectable, nameSelectable, urlSelectable)).addUrl(url)
                .addPipeline(new ConsolePipeline()).run();
    }
}
