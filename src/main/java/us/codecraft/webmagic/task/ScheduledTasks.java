package us.codecraft.webmagic.task;

import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Record;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import us.codecraft.webmagic.Spider;
import us.codecraft.webmagic.lianjia.processor.SecondHandAreaNumProcessor;
import us.codecraft.webmagic.lianjia.processor.SecondHandAreaProcessor;

import java.util.List;

@Component
public class ScheduledTasks {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Scheduled(fixedRate = 500000)
    public void reportCurrentTime() {
        //Spider.create(new SecondHandAreaProcessor()).addUrl("https://sh.lianjia.com/ershoufang/").thread(1).run();

        List<Record> areas = Db.find("select * from t_sh_second_sale_area");
        for(Record area:areas){
            Spider.create(new SecondHandAreaNumProcessor())
                    .addUrl(area.getStr("target_url"))
                    //开启5个线程抓取
                    .thread(1)
                    //启动爬虫
                    .run();
        }
    }
}


