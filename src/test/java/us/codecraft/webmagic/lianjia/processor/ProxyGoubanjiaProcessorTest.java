package us.codecraft.webmagic.lianjia.processor;

import org.junit.Test;
import us.codecraft.webmagic.Spider;

import java.util.Map;

public class ProxyGoubanjiaProcessorTest {

    @Test
    public void process() {
        ProxyGoubanjiaProcessor proxyProcessor = new ProxyGoubanjiaProcessor();
        Spider.create(proxyProcessor)
                .addUrl("https://cn-proxy.com/")
                //开启5个线程抓取
                .thread(1)
                //启动爬虫
                .run();
//
//        Map<String, Integer> proxyIpMap = proxyKuaidailiProcessor.proxyIpMap;
//
//        for (String proxyHost : ProxyValid.checkProxyIp(proxyIpMap, "https://sh.lianjia.com/").keySet()) {
//            Integer proxyPort = proxyIpMap.get(proxyHost);
//            System.out.format("%s:%s-->%s\n", proxyHost, proxyPort, "OK");
//        }
    }
}