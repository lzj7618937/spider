package us.codecraft.webmagic.lianjia.processor;

import org.junit.Test;
import us.codecraft.webmagic.Spider;

public class SecondHandProcessorTest {

    @Test
    public void process() {
        Spider.create(new SecondHandAreaProcessor())
                .addUrl("https://sh.lianjia.com/ershoufang/")
                //开启5个线程抓取
                .thread(1)
                //启动爬虫
                .run();
    }

    @Test
    public void testStr(){
        String url = "https://sh.lianjia.com/ershoufang/zhabei/pg1";
//        System.out.println(url.substring(34));
        System.out.println(url.substring(34, url.lastIndexOf("/")));
    }
}