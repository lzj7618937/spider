package us.codecraft.webmagic.lianjia.processor;

import com.jfinal.plugin.activerecord.ActiveRecordPlugin;
import com.jfinal.plugin.druid.DruidPlugin;
import org.junit.Test;
import us.codecraft.webmagic.Spider;
import us.codecraft.webmagic.lianjia.model.Area;
import us.codecraft.webmagic.lianjia.model.AreaPage;
import us.codecraft.webmagic.lianjia.model.House;
import us.codecraft.webmagic.lianjia.model.HouseUrl;

public class SecondHandAreaProcessorTest {

    @Test
    public void process() {
        DruidPlugin dp = new DruidPlugin("jdbc:mysql://localhost:3306/sblog?useUnicode=true&characterEncoding=utf8&serverTimezone=UTC", "root", "1qaz2wsx");
        ActiveRecordPlugin arp = new ActiveRecordPlugin(dp);
        arp.addMapping("t_sh_second_sale", House.class);
        arp.addMapping("t_sh_second_sale_list", HouseUrl.class);
        arp.addMapping("t_sh_second_sale_area", Area.class);
        arp.addMapping("t_sh_second_sale_area_page", AreaPage.class);
        dp.start();
        arp.start();
        Spider.create(new SecondHandAreaNumProcessor())
                .addUrl("https://sh.lianjia.com/ershoufang/zhoupu/")
                //开启5个线程抓取
                .thread(5)
                //启动爬虫
                .run();
    }

    @Test
    public void testMath(){
        int pageNum = (int) Math.ceil(179/30);
        System.out.println(pageNum);
    }
}