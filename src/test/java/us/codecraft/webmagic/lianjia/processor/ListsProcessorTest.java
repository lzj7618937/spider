package us.codecraft.webmagic.lianjia.processor;

import com.alibaba.fastjson.JSONObject;
import com.jfinal.plugin.activerecord.ActiveRecordPlugin;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Record;
import com.jfinal.plugin.druid.DruidPlugin;
import org.junit.Before;
import org.junit.Test;
import us.codecraft.webmagic.Spider;
import us.codecraft.webmagic.downloader.HttpClientDownloader;
import us.codecraft.webmagic.lianjia.model.*;
import us.codecraft.webmagic.proxy.Proxy;
import us.codecraft.webmagic.proxy.SimpleProxyProvider;

import java.util.List;
import java.util.Map;

public class ListsProcessorTest {

    @Before
    public void setUp() {
        DruidPlugin dp = new DruidPlugin("jdbc:mysql://47.99.88.16:3306/spider?autoReconnect=true&useUnicode=true&characterEncoding=utf8&useSSL=false&serverTimezone=UTC", "jay", "1qaz2wsx");
        ActiveRecordPlugin arp = new ActiveRecordPlugin(dp);
        arp.addMapping("t_lists", Lists.class);
        dp.start();
        arp.start();
    }

    @Test
    public void testProxy() {
        HttpClientDownloader httpClientDownloader = new HttpClientDownloader();
        httpClientDownloader.setProxyProvider(SimpleProxyProvider.from(
                new Proxy("43.240.5.178", 53281)));

        String domain = "https://sh.lianjia.com";
        //获取一级分类
        String url = "https://sh.lianjia.com/ershoufang/";
        String ulsSelectable = "div.position > dl:nth-child(2) > dd > div:nth-child(1) a";
        String nameSelectable = "//a/@title";
        String urlSelectable = "//a/@href";
        String type = "area";
        Spider.create(new ListsProcessor(domain, ulsSelectable, nameSelectable, urlSelectable, type))
                .setDownloader(httpClientDownloader)
                .addUrl(url)
                .thread(1).run();
    }

    @Test
    public void process() {
        String domain = "https://sh.lianjia.com";
        String ulsSelectable = "";
        String nameSelectable = "";
        String urlSelectable = "";
        String type = "";
        List<Record> recordList;
        //获取一级分类
//        String url = "https://sh.lianjia.com/ershoufang/";
//        ulsSelectable = "div.position > dl:nth-child(2) > dd > div:nth-child(1) a";
//        nameSelectable = "//a/@title";
//        urlSelectable = "//a/@href";
//        type = "area";
//        Spider.create(new ListsProcessor(domain, ulsSelectable, nameSelectable, urlSelectable, type)).addUrl(url).thread(1).run();

//        recordList = Db.find("select * from t_lists where state = 0 and type = '" + type + "'");
//        for (Record record : recordList) {
//            String recordUrl = record.getStr("url");
//            ulsSelectable = "div.position > dl:nth-child(2) > dd > div:nth-child(1) > div:nth-child(2) a";
//            nameSelectable = "//a/text()";
//            urlSelectable = "//a/@href";
//            type = "towns";
//            Spider.create(new ListsProcessor(domain, ulsSelectable, nameSelectable, urlSelectable, type)).addUrl(recordUrl).thread(1).run();
//            record.set("state", 1);
//            Db.update("t_lists", record);
//        }
//
//        type = "towns";
//        recordList = Db.find("select * from t_lists where state = 0 and type = '" + type + "'");
//        for (Record record : recordList) {
//            String recordUrl = record.getStr("url");
//            ulsSelectable = "div.leftContent > div.contentBottom.clear > div.page-box.fr > div";
//            nameSelectable = "//div/@page-data";
//            urlSelectable = "//div/@page-url";
//            type = "page";
//            Spider.create(new ListsProcessor(domain, ulsSelectable, nameSelectable, urlSelectable, type)).addUrl(recordUrl).thread(1).run();
//            record.set("state", 1);
//            Db.update("t_lists", record);
//        }

        Proxy[] proxies = ProxyValid.getKuaidailiValidProxy();
        HttpClientDownloader httpClientDownloader = new HttpClientDownloader();
        httpClientDownloader.setProxyProvider(SimpleProxyProvider.from(proxies));

        type = "page";
        recordList = Db.find("select * from t_lists where state = 0 and type = '" + type + "'");
        for (Record record : recordList) {
            JSONObject jsonObj = JSONObject.parseObject(record.getStr("name"));
            int totalPage = jsonObj.getInteger("totalPage");

            String[] urls = new String[totalPage];
            for (int i = 0; i < totalPage; i++) {
                String recordUrl = record.getStr("url").replace("{page}", (i + 1) + "co32");
                urls[i] = recordUrl;
            }
            ulsSelectable = "body > div.content > div.leftContent > ul > li > div.info.clear > div.title > a";
            nameSelectable = "//a/text()";
            urlSelectable = "//a/@href";
            type = "list";
            ListsProcessor listsProcessor = new ListsProcessor(domain, ulsSelectable, nameSelectable, urlSelectable, type);
            Spider.create(listsProcessor)
                    .addUrl(urls)
                    .setDownloader(httpClientDownloader)
                    .thread(5)
                    .run();
            record.set("state", 1);
            Db.update("t_lists", record);
        }
    }
}